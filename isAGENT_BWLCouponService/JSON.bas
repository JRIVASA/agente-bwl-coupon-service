Attribute VB_Name = "JSON"
' VBJSON is a VB6 adaptation of the VBA JSON project at http://code.google.com/p/vba-json/
' Some bugs fixed, speed improvements added for VB6 by Michael Glaser (vbjson@ediy.co.nz)
' BSD Licensed

Option Explicit

Const INVALID_JSON      As Long = 1
Const INVALID_OBJECT    As Long = 2
Const INVALID_ARRAY     As Long = 3
Const INVALID_BOOLEAN   As Long = 4
Const INVALID_NULL      As Long = 5
Const INVALID_KEY       As Long = 6
Const INVALID_RPC_CALL  As Long = 7

Private psErrors As String

Public Function GetParserErrors() As String
   GetParserErrors = psErrors
End Function

Public Function ClearParserErrors() As String
   psErrors = Empty
End Function

'
'   Parse String And Create JSON Object
'

Public Function Parse(ByRef pStr As String) As Object
    
    Dim Index As Long
    
    Index = 1
    
    psErrors = Empty
    
    On Error Resume Next
    
    Call SkipChar(pStr, Index)
    
    Select Case Mid(pStr, Index, 1)
        Case "{"
            Set Parse = ParseObject(pStr, Index)
        Case "["
            Set Parse = ParseArray(pStr, Index)
        Case Else
            psErrors = "Invalid JSON"
    End Select
    
End Function

'Ejemplos de como procesar las respuestas.
'Respuesta modelo:
'{"rastreos":[{"messageId":"011101877880a1f2-82560db4-fd13-46b8-94fd-d4b7f504b854-000000","correo":"PRUEBA@ALGO.COM","status":"Mensaje de Correo Electrónico entregado exitosamente.","fecha":"2023-04-12T22:45:09-04:00"}],"codigo":"200","mensaje":"Proceso completo","validaciones":["un nodo sin nombre","otro nodo sin nombre"]}

'JsonString = "{""rastreos"":[{""messageId"":""011101877880a1f2-82560db4-fd13-46b8-94fd-d4b7f504b854-000000"",""correo"":""PRUEBA@ALGO.COM"",""status"":""Mensaje de Correo Electrónico entregado exitosamente."",""fecha"":""2023-04-12T22:45:09-04:00""}],""codigo"":""200"",""mensaje"":""Proceso completo"",""validaciones"":[""un nodo sin nombre"",""otro nodo sin nombre""]}"
'Set RespObj = JSON.parse(JsonString)

'1) Como acceder a una propiedad con nombre.
'R: Dato = RespObj("mensaje") -> Obtiene la propiedad del atributo mensaje en la raiz de RespObj
'2) Como acceder a una propiedad tipo lista.
'R: Lista = RespObj("rastreos") -> Devuelve un Collection que se puede recorrer por indice o con for each
'CantItemsLista = Lista.Count
'3) Como acceder a un elemento de la lista
'R: ListaPrimerItem = Lista(1). ListaSegundoItem_messageId = Lista(2)("messageId")
'4) Como iterar la lista
'R: ForEach Item In Lista
'       messageId = Item("messageId")
'       ...
'   Next
'O con un For Normal:
'   For I = 1 To Lista.Count
'       messageId = Lista(I)("messageId")
'   Next
'5) Se pueden contar la lista de nodo de un Nodo (independientemente de que tenga Listas o Propiedades o Ambos) ?
'R: Si
'   CantHijos = RespObj.Count -> 3 (rastreos, codigo y mensaje)
'   CantHijosLista = RespObj("rastreos").Count -> 1 (rastreos es una lista pero solo tiene un nodo)
'   CantPropiedadesHijo = RespObj("rastreos")(1).Count -> 4 (messageId, correo, status y fecha)
'6) Como recorrer una lista que tiene elementos simples (sin nombres de propiedad)
'R:  Igual que en las R 3) y 4), solo que sin especificar nombre de elemento.
'Ej: DatoPrimerItem = RespObj("validaciones")(1)
'    DatoSegundoItem = RespObj("validaciones")(2)
'    Iterar RespObj("validaciones"):
'    ForEach Item In RespObj("validaciones")
'        Debug.Print Item
'    Next
'O:
'    For I = 1 To RespObj("validaciones").Count
'        Debug.Print RespObj("validaciones")(I)
'    Next

'
'   parse collection of key/value
'

Private Function ParseObject(ByRef pStr As String, ByRef Index As Long) As Dictionary
     
    Set ParseObject = New Dictionary
    
    Dim sKey As String
    
    ' "{"
    
    Call SkipChar(pStr, Index)
    
    If Mid(pStr, Index, 1) <> "{" Then
        psErrors = psErrors & "Invalid Object at position " & Index & " : " & Mid(pStr, Index) & vbCrLf
        Exit Function
    End If
    
    Index = Index + 1
     
    Do
        
        Call SkipChar(pStr, Index)
        
        If "}" = Mid(pStr, Index, 1) Then
           Index = Index + 1
           Exit Do
        ElseIf "," = Mid(pStr, Index, 1) Then
           Index = Index + 1
           Call SkipChar(pStr, Index)
        ElseIf Index > Len(pStr) Then
           psErrors = psErrors & "Missing '}': " & Right(pStr, 20) & vbCrLf
           Exit Do
        End If
        
        ' add key/value pair
        sKey = ParseKey(pStr, Index)
        
        On Error Resume Next
        
        ParseObject.Add sKey, parseValue(pStr, Index)
        
        If Err.Number <> 0 Then
           psErrors = psErrors & Err.Description & ": " & sKey & vbCrLf
           Exit Do
        End If
        
    Loop
    
End Function

'
'   Parse List
'

Private Function ParseArray(ByRef pStr As String, ByRef Index As Long) As Collection
    
    Set ParseArray = New Collection
    
    ' "["
    
    Call SkipChar(pStr, Index)
    
    If Mid(pStr, Index, 1) <> "[" Then
        psErrors = psErrors & "Invalid Array at position " & Index & " : " + Mid(pStr, Index, 20) & vbCrLf
        Exit Function
    End If
    
    Index = Index + 1
    
    Do
        
        Call SkipChar(pStr, Index)
        
        If "]" = Mid(pStr, Index, 1) Then
           Index = Index + 1
           Exit Do
        ElseIf "," = Mid(pStr, Index, 1) Then
           Index = Index + 1
           Call SkipChar(pStr, Index)
        ElseIf Index > Len(pStr) Then
           psErrors = psErrors & "Missing ']': " & Right(pStr, 20) & vbCrLf
           Exit Do
        End If
        
        ' add value
        
        On Error Resume Next
        
        ParseArray.Add parseValue(pStr, Index)
        
        If Err.Number <> 0 Then
           psErrors = psErrors & Err.Description & ": " & Mid(pStr, Index, 20) & vbCrLf
           Exit Do
        End If
        
    Loop
    
End Function

'
'   Parse String / Number / Object / Array / true / false / null
'

Private Function parseValue(ByRef pStr As String, ByRef Index As Long)
    
    Call SkipChar(pStr, Index)
    
    Select Case Mid(pStr, Index, 1)
        Case "{"
            Set parseValue = ParseObject(pStr, Index)
        Case "["
            Set parseValue = ParseArray(pStr, Index)
        Case """", "'"
            parseValue = ParseString(pStr, Index)
        Case "t", "f"
            parseValue = ParseBoolean(pStr, Index)
        Case "n"
            parseValue = ParseNull(pStr, Index)
        Case Else
            parseValue = ParseNumber(pStr, Index)
    End Select
    
End Function

'
'   Parse String
'

Private Function ParseString(ByRef pStr As String, ByRef Index As Long) As String
    
    Dim Quote   As String
    Dim Char    As String
    Dim Code    As String
    
    Dim sB As New cStringBuilder
    
    Call SkipChar(pStr, Index)
    
    Quote = Mid(pStr, Index, 1)
    
    Index = Index + 1
    
    Do While Index > 0 _
    And Index <= Len(pStr)
        
        Char = Mid(pStr, Index, 1)
        
        Select Case (Char)
            
            Case "\"
                
                Index = Index + 1
                Char = Mid(pStr, Index, 1)
                
                Select Case (Char)
                    Case """", "\", "/", "'"
                        sB.Append Char
                        Index = Index + 1
                    Case "b"
                        sB.Append vbBack
                        Index = Index + 1
                    Case "f"
                        sB.Append vbFormFeed
                        Index = Index + 1
                    Case "n"
                        sB.Append vbLf
                        Index = Index + 1
                    Case "r"
                        sB.Append vbCr
                        Index = Index + 1
                    Case "t"
                        sB.Append vbTab
                        Index = Index + 1
                    Case "u"
                        Index = Index + 1
                        Code = Mid(pStr, Index, 4)
                        sB.Append ChrW(Val("&h" + Code))
                        Index = Index + 4
                End Select
                
            Case Quote
                
                Index = Index + 1
                
                ParseString = sB.ToString
                
                Set sB = Nothing
                
                Exit Function
                
            Case Else
                
                sB.Append Char
                
                Index = Index + 1
                
        End Select
        
    Loop
    
    ParseString = sB.ToString
    Set sB = Nothing
    
End Function

'
'   Parse Number
'

Private Function ParseNumber(ByRef pStr As String, ByRef Index As Long) As Variant
    
    Dim Value   As String
    Dim Char    As String
    
    Call SkipChar(pStr, Index)
    
    Do While Index > 0 And Index <= Len(pStr)
        
        Char = Mid(pStr, Index, 1)
        
        If InStr("+-0123456789.eE", Char) Then
            Value = Value & Char
            Index = Index + 1
        Else
            ParseNumber = CDec(Value)
            Exit Function
        End If
        
    Loop
    
End Function

'
'   Parse true / false
'

Private Function ParseBoolean(ByRef pStr As String, ByRef Index As Long) As Boolean
    
    Call SkipChar(pStr, Index)
    
    If Mid(pStr, Index, 4) = "true" Then
        ParseBoolean = True
        Index = Index + 4
    ElseIf Mid(pStr, Index, 5) = "false" Then
        ParseBoolean = False
        Index = Index + 5
    Else
        psErrors = psErrors & "Invalid Boolean at position " & Index & " : " & Mid(pStr, Index) & vbCrLf
    End If
    
End Function

'
'   Parse null
'

Private Function ParseNull(ByRef pStr As String, ByRef Index As Long)
    
    Call SkipChar(pStr, Index)
    
    If Mid(pStr, Index, 4) = "null" Then
        ParseNull = Null
        Index = Index + 4
    Else
        psErrors = psErrors & "Invalid null value at position " & Index & " : " & Mid(pStr, Index) & vbCrLf
    End If
    
End Function

Private Function ParseKey(ByRef pStr As String, ByRef Index As Long) As String
    
    Dim dQuote  As Boolean
    Dim sQuote  As Boolean
    Dim Char    As String
    
    Call SkipChar(pStr, Index)
    
    Do While Index > 0 _
    And Index <= Len(pStr)
        
        Char = Mid(pStr, Index, 1)
        
        Select Case (Char)
            
            Case """"
                
                dQuote = Not dQuote
                Index = Index + 1
                
                If Not dQuote Then
                    
                    Call SkipChar(pStr, Index)
                    
                    If Mid(pStr, Index, 1) <> ":" Then
                        psErrors = psErrors & "Invalid Key at position " & Index & " : " & ParseKey & vbCrLf
                        Exit Do
                    End If
                    
                End If
                
            Case "'"
                
                sQuote = Not sQuote
                Index = Index + 1
                
                If Not sQuote Then
                    
                    Call SkipChar(pStr, Index)
                    
                    If Mid(pStr, Index, 1) <> ":" Then
                        psErrors = psErrors & "Invalid Key at position " & Index & " : " & ParseKey & vbCrLf
                        Exit Do
                    End If
                    
                End If
                
            Case ":"
                
                Index = Index + 1
                
                If Not dQuote And Not sQuote Then
                    Exit Do
                Else
                    ParseKey = ParseKey & Char
                End If
                
            Case Else
                
                If InStr(vbCrLf & vbCr & vbLf & vbTab & " ", Char) Then
                
                Else
                    ParseKey = ParseKey & Char
                End If
                
                Index = Index + 1
                
        End Select
        
    Loop
    
End Function

'
'   Skip Special Character
'

Private Sub SkipChar(ByRef pStr As String, ByRef Index As Long)
    
    Dim bComment As Boolean
    Dim bStartComment As Boolean
    Dim bLongComment As Boolean
    
    Do While Index > 0 _
    And Index <= Len(pStr)
        
        Select Case Mid(pStr, Index, 1)
            
            Case vbCr, vbLf
                
                If Not bLongComment Then
                    bStartComment = False
                    bComment = False
                End If
                
            Case vbTab, " ", "(", ")"
                
            Case "/"
                
                If Not bLongComment Then
                    
                    If bStartComment Then
                        bStartComment = False
                        bComment = True
                    Else
                        bStartComment = True
                        bComment = False
                        bLongComment = False
                    End If
                    
                Else
                    
                    If bStartComment Then
                        bLongComment = False
                        bStartComment = False
                        bComment = False
                    End If
                    
                End If
                
            Case "*"
                
                If bStartComment Then
                    bStartComment = False
                    bComment = True
                    bLongComment = True
                Else
                    bStartComment = True
                End If
                
            Case Else
                
                If Not bComment Then
                    Exit Do
                End If
                
        End Select
        
        Index = Index + 1
        
    Loop
    
End Sub

Public Function ToString(ByRef pObj As Variant) As String
    
    Dim sB As New cStringBuilder
    
    Select Case VarType(pObj)
        
        Case vbNull
            sB.Append "null"
        Case vbDate
            sB.Append """" & CStr(pObj) & """"
        Case vbString
            sB.Append """" & Encode(pObj) & """"
        Case vbObject
            
            Dim bFI As Boolean
            Dim I As Long
            
            bFI = True
            
            If TypeName(pObj) = "Dictionary" Then
                
                sB.Append "{"
                
                Dim Keys
                
                Keys = pObj.Keys
                
                For I = 0 To pObj.Count - 1
                    
                    If bFI Then
                        bFI = False
                    Else
                        sB.Append ","
                    End If
                    
                    Dim Key
                    Key = Keys(I)
                    sB.Append """" & Key & """:" & ToString(pObj.Item(Key))
                    
                Next I
                
                sB.Append "}"
                
            ElseIf TypeName(pObj) = "Collection" Then
                
                sB.Append "["
                
                Dim Value
                
                For Each Value In pObj
                    
                    If bFI Then
                        bFI = False
                    Else
                        sB.Append ","
                    End If
                    
                    sB.Append ToString(Value)
                    
                Next Value
                
                sB.Append "]"
                
            End If
            
        Case vbBoolean
           
            If pObj Then
                sB.Append "true"
            Else
                sB.Append "false"
            End If
            
        Case vbVariant, vbArray, vbArray + vbVariant
            
            Dim sEB
            
            sB.Append MultiArray(pObj, 1, vbNullString, sEB)
            
        Case Else
            
            sB.Append Replace(pObj, ",", ".")
            
    End Select
    
    ToString = sB.ToString
    
    Set sB = Nothing
    
End Function

Private Function Encode(pStr) As String
    
    Dim sB As New cStringBuilder
    Dim I As Long
    Dim J As Long
    Dim AL1 As Variant
    Dim AL2 As Variant
    Dim C As String
    Dim P As Boolean
    
    AL1 = Array(&H22, &H5C, &H2F, &H8, &HC, &HA, &HD, &H9)
    AL2 = Array(&H22, &H5C, &H2F, &H62, &H66, &H6E, &H72, &H74)
    
    For I = 1 To Len(pStr)
        
        P = True
        
        C = Mid(pStr, I, 1)
        
        For J = 0 To 7
            If C = Chr(AL1(J)) Then
                sB.Append "\" & Chr(AL2(J))
                P = False
                Exit For
            End If
        Next
        
        If P Then
            
            Dim A
            
            A = AscW(C)
            
            If A > 31 And A < 127 Then
                sB.Append C
            ElseIf A > -1 Or A < 65535 Then
                sB.Append "\u" & String(4 - Len(Hex(A)), "0") & Hex(A)
            End If
            
        End If
        
    Next
    
    Encode = sB.ToString
    
    Set sB = Nothing
    
End Function

Private Function MultiArray(aBD, iBC, sPS, ByRef sPT)   ' Array BoDy, Integer BaseCount, String PoSition
    
    Dim iDU As Long
    Dim iDL As Long
    
    Dim I As Long
    
    On Error Resume Next
    
    iDL = LBound(aBD, iBC)
    iDU = UBound(aBD, iBC)
    
    Dim sB As New cStringBuilder
    
    Dim sPB1, sPB2  ' String PointBuffer1, String PointBuffer2
    
    If Err.Number = 9 Then
        
        sPB1 = sPT & sPS
        
        For I = 1 To Len(sPB1)
            
            If I <> 1 Then
                sPB2 = sPB2 & ","
            End If
            
            sPB2 = sPB2 & Mid(sPB1, I, 1)
            
        Next
        
        '        multiArray = multiArray & toString(Eval("aBD(" & sPB2 & ")"))
        
        sB.Append ToString(aBD(sPB2))
        
    Else
        
        sPT = sPT & sPS
        
        sB.Append "["
        
        For I = iDL To iDU
            
            sB.Append MultiArray(aBD, iBC + 1, I, sPT)
            
            If I < iDU Then
                sB.Append ","
            End If
            
        Next
        
        sB.Append "]"
        
        sPT = Left(sPT, iBC - 2)
        
    End If
    
    Err.Clear
    
    MultiArray = sB.ToString
    
    Set sB = Nothing
    
End Function

' Miscellaneous JSON functions

Public Function StringToJSON(pStr As String) As String
    
    Const FIELD_SEP = "~"
    Const RECORD_SEP = "|"
    
    Dim sFlds As String
    Dim sRecs As New cStringBuilder
    Dim lRecCnt As Long
    Dim lFld As Long
    Dim Fld As Variant
    Dim Rows As Variant
    
    lRecCnt = 0
    
    If pStr = Empty Then
        
        StringToJSON = "null"
        
    Else
        
        Rows = Split(pStr, RECORD_SEP)
        
        For lRecCnt = LBound(Rows) To UBound(Rows)
            
            sFlds = Empty
            
            Fld = Split(Rows(lRecCnt), FIELD_SEP)
            
            For lFld = LBound(Fld) To UBound(Fld) Step 2
                
                sFlds = (sFlds & IIf(sFlds <> "", ",", "") & """" & Fld(lFld) & """:""" & ToUnicode(Fld(lFld + 1) & "") & """")
                
            Next 'fld
            
            sRecs.Append IIf((Trim(sRecs.ToString) <> ""), "," & vbCrLf, "") & "{" & sFlds & "}"
            
        Next 'rec
        
        StringToJSON = ("( {""Records"": [" & vbCrLf & sRecs.ToString & vbCrLf & "], " & """RecordCount"":""" & lRecCnt & """ } )")
        
    End If
    
End Function

Public Function RStoJSON(Rs As ADODB.Recordset) As String
    
    On Error GoTo ErrHandler
    
    Dim sFlds As String
    Dim sRecs As New cStringBuilder
    Dim lRecCnt As Long
    Dim Fld As ADODB.Field
    
    lRecCnt = 0
    
    If Rs.State = adStateClosed Then
        
        RStoJSON = "null"
        
    Else
        
        If Rs.EOF Or Rs.BOF Then
            
            RStoJSON = "null"
            
        Else
            
            Do While Not Rs.EOF And Not Rs.BOF
                
                lRecCnt = lRecCnt + 1
                
                sFlds = Empty
                
                For Each Fld In Rs.Fields
                    sFlds = (sFlds & IIf(sFlds <> "", ",", "") & """" & Fld.Name & """:""" & ToUnicode(Fld.Value & "") & """")
                Next 'fld
                
                sRecs.Append IIf((Trim(sRecs.ToString) <> ""), "," & vbCrLf, "") & "{" & sFlds & "}"
                
                Rs.MoveNext
                
            Loop
            
            RStoJSON = ("( {""Records"": [" & vbCrLf & sRecs.ToString & vbCrLf & "], " & """RecordCount"":""" & lRecCnt & """ } )")
            
       End If
       
    End If
    
    Exit Function
    
ErrHandler:
    
End Function

Public Function ToUnicode(pStr As String) As String
    
    Dim X As Long
    Dim uStr As New cStringBuilder
    Dim uChrCode As Integer
    
    For X = 1 To Len(pStr)
        
        uChrCode = Asc(Mid(pStr, X, 1))
        
        Select Case uChrCode
            Case 8:   ' backspace
                uStr.Append "\b"
            Case 9: ' tab
                uStr.Append "\t"
            Case 10:  ' line feed
                uStr.Append "\n"
            Case 12:  ' formfeed
                uStr.Append "\f"
            Case 13: ' carriage return
                uStr.Append "\r"
            Case 34: ' quote
                uStr.Append "\"""
            Case 39:  ' apostrophe
                uStr.Append "\'"
            Case 92: ' backslash
                uStr.Append "\\"
            Case 123, 125:  ' "{" and "}"
                uStr.Append ("\u" & Right("0000" & Hex(uChrCode), 4))
            Case Is < 32, Is > 127: ' non-ascii characters
                uStr.Append ("\u" & Right("0000" & Hex(uChrCode), 4))
            Case Else
                uStr.Append Chr$(uChrCode)
        End Select
        
    Next
    
    ToUnicode = uStr.ToString
    
End Function

Private Sub Class_Initialize()
   
   psErrors = Empty
   
End Sub
