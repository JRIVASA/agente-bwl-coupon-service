Attribute VB_Name = "ModuloPrincipal"
Public Enum TipoInterfaz
    Agente = 1
    AppBusiness = 2
    AppPos = 3
    [ItmCount]
End Enum

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String

Global Srv_Remote_BD_ADM As String, Srv_Remote_BD_POS As String
Global gSucursal As String

Global gVerificacionElectro As Object ' Verificacion Electronica

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Global Setup As String

Global gModalidad As TipoInterfaz

Global CodigoCuponCompra                                        As String
Global CodigoCuponNotaCredito                                   As String
Global DenominacionCuponCompra                                  As String
Global DenominacionCuponNotaCredito                             As String
Global IDCuenta_BWL_CS                                          As String
Global HostName_BWL_CS                                          As String
Global BWL_CS_CompatibilityVersion                              As Double
Global BWL_CS_ServiceVersion                                    As Double
Global BWL_CS_Locale                                            As String
Global BWL_CS_ApiUser                                           As String
Global BWL_CS_ApiKey                                            As String
Global BWL_CS_SrvPass_Alterna                                   As String

Global StellarWallet_DenominacionPorMoneda                      As Dictionary
Global StellarWallet_TipoWalletPorDenominacion                  As Dictionary
Global StellarWallet_FormaDePagoPorQR                           As Boolean
Global StellarWallet_CodigoBancoRetorno                         As String
Global StellarWallet_IDClienteAuto                              As Boolean

Public Moneda_Cod As String
Public Moneda_Fac As Double
Public Moneda_Des As String
Public Moneda_Dec As Double
Public Moneda_Sim As String
Public Moneda_ISO As String

Private Sub Main()
    
    Setup = App.Path & "\Setup.ini"
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(21) & Chr(55) & Chr(4) & Chr(51) _
    & Chr(88) & Chr(72) & Chr(49) & Chr(77) _
    & Chr(89) & Chr(68) & Chr(65) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(21) & "-" _
    & Chr(33) & Chr(48) & Chr(72) & Chr(71) _
    & Chr(52) & Chr(44) & Chr(48) & "_" & Chr(90)
    
    Dim Funciones As New Funciones
    
    Call Funciones.Ejecutar
    
End Sub

' Quick Syntax

'LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al ...ESPECIFICAR... : " & Err.Description & " " & "(" & Err.Number & ")[" & Err.Source & "]"
'LogFile LogContent

Public Sub LogFile(ByVal pText As String, _
Optional pDir As String = "$(AppPath)", _
Optional pName As String = "$(FileName)" _
)
    
    On Error GoTo Error
    
    pDir = Replace(pDir, "$(AppPath)", App.Path)
    pName = Replace(pName, "$(FileName)", App.EXEName & ".log")
    
    TmpFile = FreeFile
    
    Open pDir & "\" & pName For Append As TmpFile
    
    Print #TmpFile, pText
    
    Close TmpFile
    
    Exit Sub
    
Error:
    
End Sub

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
    Err.Clear
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Function LoadTextFile(ByVal pFilePath As String) As String
    
    On Error GoTo Error
    
    FrmLoadTextFile.RDoc.LoadFile pFilePath, rtfText
    Debug.Print Len(FrmLoadTextFile.RDoc.Text)
    LoadTextFile = FrmLoadTextFile.RDoc.Text
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

Public Function SaveTextFile(ByVal pContents As String, ByVal pFilePath As String, _
Optional ByVal pUseUnicode As Boolean = False) As Boolean
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.CreateTextFile(pFilePath, True, pUseUnicode)
    TStream.Write (pContents)
    TStream.Close
    
    WriteStringIntoFile = True

    Exit Function
    
ErrFile:
    
    Debug.Print Err.Description
    
End Function


Public Function PathExists(pPath As String) As Boolean
    On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then
        If Err.Number = 0 Then
            PathExists = True
        End If
    End If
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim POS As Long
    
    POS = InStr(1, pPath, "\")
    
    If POS <> 0 Then
        GetDirectoryRoot = Left(pPath, POS - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(ByVal pPath As String, _
Optional ByVal pURL As Boolean) As String

    Dim POS As Long
    
    POS = InStrRev(pPath, IIf(pURL, "/", "\"))
    
    If POS <> 0 Then
        GetDirParent = Left(pPath, POS - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For I = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next I
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For I = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(I)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next I
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function

Public Function KillSecure(ByVal pFile As String) As Boolean
    On Error GoTo KS
    Kill pFile: KillSecure = True
    Exit Function
KS:
    'Debug.Print Err.Description
    Err.Clear 'Ignorar.
End Function

Public Function KillShot(ByVal pFile As String) As Boolean
    KillShot = KillSecure(pFile)
End Function

Public Function ReadFileIntoString(StrFilePath As String) As String
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.OpenTextFile(StrFilePath)
    If Not TStream.AtEndOfStream Then ReadFileIntoString = TStream.ReadAll

    Exit Function
    
ErrFile:
    
    ReadFileIntoString = ""
    
End Function

Public Function ExecuteSafeSQL(ByVal SQL As String, _
pCn As ADODB.Connection, _
Optional ByRef Records = 0, _
Optional ByVal ReturnsResultSet As Boolean = False, _
Optional ByVal DisconnectedRS As Boolean = False, _
Optional ByVal Updatable As Boolean = False) As ADODB.Recordset
    
    On Error GoTo Error
    
    If ReturnsResultSet Then
        
        If DisconnectedRS Or Updatable Then
            
            Set ExecuteSafeSQL = New ADODB.Recordset
            
            ExecuteSafeSQL.CursorLocation = adUseClient
            ExecuteSafeSQL.Open SQL, pCn, adOpenStatic, _
            IIf(Updatable, adLockBatchOptimistic, adLockReadOnly), adCmdText
            
            If DisconnectedRS Then
                Set ExecuteSafeSQL.ActiveConnection = Nothing
            End If
            
            Records = ExecuteSafeSQL.RecordCount
            
        Else
            
            Set ExecuteSafeSQL = pCn.Execute(SQL, Records)
            
        End If
        
    Else
        
        pCn.Execute SQL, Records
        Set ExecuteSafeSQL = Nothing
        
    End If
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    'If DebugModeExtendido Then
        'MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").", "(ExecuteSafeSQL)" & GetLines(2) & SQL & GetLines(2)
    'End If
    
    Records = -1
    
    Set ExecuteSafeSQL = Nothing
    
End Function

Public Sub Open_Rec(Rec_Local As Boolean, Reg As ADODB.Recordset)

    If Reg.State = adStateOpen Then Reg.Close
    
    If Rec_Local Then
        Reg.CursorLocation = adUseClient
    Else
        Reg.CursorLocation = adUseServer
    End If
    
End Sub

Public Sub Close_Rec(Reg As ADODB.Recordset)
    If Reg.State = adStateOpen Then Reg.Close
    Set Reg = Nothing
End Sub

Public Function AsEnumerable(ArrayList As Variant, _
Optional ByVal pCtrlError As Boolean = True) As Collection
    
    Set AsEnumerable = New Collection
    
    If pCtrlError Then On Error Resume Next
    
    For I = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(I), CStr(I)
    Next I
    
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function SafeEquals(ByVal pVal1, ByVal pVal2) As Boolean
    On Error Resume Next
    SafeEquals = (pVal1 = pVal2) ' Attempt to Compare Values.
    If Err.Number = 0 Then Exit Function
    SafeEquals = (pVal1 Is pVal2) ' Attempt to Compare Object References.
End Function

' Changed AS a Pointer to V2.

Public Function Collection_AddKey(pCollection As Collection, ByVal pValor, ByVal pKey As String) As Boolean
    Collection_AddKey = Collection_SafeAddKey(pCollection, pValor, pKey)
End Function

' To attempt to add Item + Key and ignore error in case Key is already contained in the collection.
' Ignore duplication error.

Public Function Collection_SafeAddKey(pCollection As Collection, ByVal pValue, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Add pValue, pKey
    
    Collection_SafeAddKey = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_EncontrarValor(pCollection As Collection, ByVal pValor, Optional ByVal pIndiceInicial As Long = 1) As Long
    Collection_EncontrarValor = Collection_FindValueIndex(pCollection, pValor, pIndiceInicial)
End Function

' How to Use:

' 1) To Know if Value Exists:

'If Collection_FindValueIndex(pCollection, pValue) <> -1 Then

' 2) To Search All Instances (Indexes) of Value in the Collection. Quick Example:

' SearchIndex = Collection_FindValueIndex(pCollection, pValue) ' Start Looking from Index 1

' If SearchIndex <> -1 Then
    ' This should be in a kind of While Loop...
    ' Keep searching other matches starting from next position:
    'SearchIndex = Collection_FindValueIndex(pCollection, pValue, SearchIndex + 1)
    ' Until Search Index = -1
' Else
    ' Next Match not found...
' End if

Public Function Collection_FindValueIndex(pCollection As Collection, ByVal pValue, Optional ByVal pStartIndex As Long = 1) As Long
    
    On Error GoTo Error
    
    Dim I As Long
    
    Collection_FindValueIndex = -1
    
    For I = pStartIndex To pCollection.Count
        If SafeEquals(pCollection.item(I), pValue) Then
            Collection_FindValueIndex = I
            Exit Function
        End If
    Next I
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteKey(pCollection As Collection, ByVal pKey As String, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteKey = Collection_HasKey(pCollection, pKey)
End Function

' Collection_ExisteKey V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pKey, Null)) Then ' Key might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.item(pKey) ' If this doesn't Proc Error Key is Null, but exists.
        Collection_HasKey = True
    Else ' Key Exists AND <> NULL
        Collection_HasKey = True
    End If
    
    Exit Function
        
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasKey = False ' Only on error, Key is not contained in the Collection.
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteIndex(pCollection As Collection, ByVal pIndex As Long, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteIndex = Collection_HasIndex(pCollection, pIndex)
End Function

' Collection_ExisteIndex V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pIndex, Null)) Then ' Index might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.item(pIndex) ' If this doesn't Proc Error Index also exists.
        Collection_HasIndex = True
    Else ' Index Exists AND <> NULL
        Collection_HasIndex = True
    End If
    
    Exit Function
    
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasIndex = False ' Only on error Index is not contained in the Collection.
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean
    Collection_RemoveKey = Collection_SafeRemoveKey(pCollection, pKey)
End Function

' Ignore error even if the Key is not contained in the Collection.

Public Function Collection_SafeRemoveKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pKey
        
    Collection_SafeRemoveKey = True
     
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveKey = False ' Non - existant.
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    Collection_RemoveIndex = Collection_SafeRemoveIndex(pCollection, pIndex)
End Function

' Ignore error even if the Index is not contained in the Collection.

Public Function Collection_SafeRemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pIndex
    
    Collection_SafeRemoveIndex = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveIndex = False ' Non - existant.
    
End Function

Public Function ConvertirCadenaEnLista(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pPermitirDuplicados As Boolean = False) As Collection
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    Set ConvertirCadenaEnLista = New Collection
    
    Dim item, Clave
    
    For Each item In AsEnumerable(Split(pCadena, pSeparador))
        
        Clave = CStr(item)
        
        If pKeysCaseOption = 1 Then Clave = LCase(Clave)
        If pKeysCaseOption = 2 Then Clave = UCase(Clave)
        If pKeysCaseOption = 3 Then Clave = StrConv(Clave, vbProperCase)
        
        If pPermitirDuplicados Then
            ConvertirCadenaEnLista.Add Clave
        Else
            Collection_SafeAddKey ConvertirCadenaEnLista, Clave, Clave
        End If
        
    Next
    
    If ConvertirCadenaEnLista.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaEnLista = Nothing
    
End Function

Public Function StringToList(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pPermitirDuplicados As Boolean = False) As Collection
    Set StringToList = ConvertirCadenaEnLista(pCadena, pSeparador, pKeysCaseOption, pPermitirDuplicados)
End Function

Public Function ConvertirCadenaDeAsociacion(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pValuesCaseOption As Integer = 0) As Collection
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacion = New Collection
    
    Dim item, ParClaveValor
    
    For Each item In AsEnumerable(Split(pCadena, pSeparador))
        
        ParClaveValor = Split(CStr(item), ":", 2)               'Izq:Clave        'Der:Valor
        
        If pKeysCaseOption = 1 Then ParClaveValor(0) = LCase(ParClaveValor(0))
        If pKeysCaseOption = 2 Then ParClaveValor(0) = UCase(ParClaveValor(0))
        If pKeysCaseOption = 3 Then ParClaveValor(0) = StrConv(ParClaveValor(0), vbProperCase)
        
        If pValuesCaseOption = 1 Then ParClaveValor(1) = LCase(ParClaveValor(1))
        If pValuesCaseOption = 2 Then ParClaveValor(1) = UCase(ParClaveValor(1))
        If pValuesCaseOption = 3 Then ParClaveValor(1) = StrConv(ParClaveValor(1), vbProperCase)
        
        Call Collection_SafeAddKey(ConvertirCadenaDeAsociacion, ParClaveValor(1), ParClaveValor(0))
        
    Next
    
    If ConvertirCadenaDeAsociacion.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacion = Nothing
    
End Function

Public Function ConvertirCadenaDeAsociacionAvanzado(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pValuesCaseOption As Integer = 0) As Dictionary
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacionAvanzado = New Dictionary
    
    Dim item, ParClaveValor
    
    For Each item In AsEnumerable(Split(pCadena, pSeparador))
        
        ParClaveValor = Split(CStr(item), pSeparadorInterno, 2) ' Izq -> Clave ' Der -> Valor
        
        If pKeysCaseOption = 1 Then ParClaveValor(0) = LCase(ParClaveValor(0))
        If pKeysCaseOption = 2 Then ParClaveValor(0) = UCase(ParClaveValor(0))
        If pKeysCaseOption = 3 Then ParClaveValor(0) = StrConv(ParClaveValor(0), vbProperCase)
        
        If pValuesCaseOption = 1 Then ParClaveValor(1) = LCase(ParClaveValor(1))
        If pValuesCaseOption = 2 Then ParClaveValor(1) = UCase(ParClaveValor(1))
        If pValuesCaseOption = 3 Then ParClaveValor(1) = StrConv(ParClaveValor(1), vbProperCase)
        
        If Not ConvertirCadenaDeAsociacionAvanzado.Exists(ParClaveValor(0)) Then
            ConvertirCadenaDeAsociacionAvanzado.Add ParClaveValor(0), ParClaveValor(1)
        End If
        
    Next
    
    If ConvertirCadenaDeAsociacionAvanzado.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacionAvanzado = Nothing
    
End Function

Public Function ExportarCadenaDeAsociacion(ByVal pCollection As Collection, _
Optional ByVal pSeparador As String = "|") As String
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    ExportarCadenaDeAsociacion = Empty
    
    Dim item
    
    If pCollection.Count <= 0 Then Exit Function
    
    For Each item In pCollection
        ExportarCadenaDeAsociacion = ExportarCadenaDeAsociacion & _
        pSeparador & item
    Next
    
    ExportarCadenaDeAsociacion = Mid(ExportarCadenaDeAsociacion, 2)
    
    Exit Function
    
Error:
    
    ExportarCadenaDeAsociacion = Empty
    
End Function

Public Function ExportarCadenaDeAsociacionAvanzado(ByVal pValues As Dictionary, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":") As String
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    ExportarCadenaDeAsociacionAvanzado = Empty
    
    Dim ItemKey, KeysCollection
    
    Set KeysCollection = AsEnumerable(pValues.Keys())
    
    If KeysCollection.Count <= 0 Then Exit Function
    
    For Each ItemKey In KeysCollection
        ExportarCadenaDeAsociacionAvanzado = ExportarCadenaDeAsociacionAvanzado & _
        pSeparador & ItemKey & pSeparadorInterno & pValues.item(ItemKey)
    Next
    
    ExportarCadenaDeAsociacionAvanzado = Mid(ExportarCadenaDeAsociacionAvanzado, 2)
    
    Exit Function
    
Error:
    
    ExportarCadenaDeAsociacionAvanzado = Empty
    
End Function

Public Function HexToString(ByVal HexToStr As String) As String
    
    Dim strTemp   As String
    Dim StrReturn As String
    Dim I         As Long
    
    For I = 1 To Len(HexToStr) Step 2
        strTemp = Chr$(Val("&H" & Mid$(HexToStr, I, 2)))
        StrReturn = StrReturn & strTemp
    Next I
    
    HexToString = StrReturn
    
End Function

Public Function StringToHex(ByVal StrToHex As String) As String
    
    Dim strTemp   As String
    Dim StrReturn As String
    Dim I         As Long
    
    For I = 1 To Len(StrToHex)
        strTemp = Hex$(Asc(Mid$(StrToHex, I, 1)))
        If Len(strTemp) = 1 Then strTemp = "0" & strTemp
        StrReturn = StrReturn & Space$(1) & strTemp
    Next I
    
    StringToHex = StrReturn
    
End Function
