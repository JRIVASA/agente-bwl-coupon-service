VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cJSONScript"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim DictVars As New Dictionary
Dim plNestCount As Long

Public Function Eval(sJson As String) As String
    
    Dim sB As New cStringBuilder
    Dim O As Object
    Dim C As Object
    Dim I As Long
    
    Set O = JSON.Parse(sJson)
    
    If (JSON.GetParserErrors = Empty) _
    And Not (O Is Nothing) Then
        
        For I = 1 To O.Count
            
            Select Case VarType(O.Item(I))
                Case vbNull
                    sB.Append "null"
                Case vbDate
                    sB.Append CStr(O.Item(I))
                Case vbString
                    sB.Append CStr(O.Item(I))
                Case Else
                    Set C = O.Item(I)
                    sB.Append ExecCommand(C)
            End Select
            
        Next
        
    Else
       'MsgBox JSON.GetParserErrors, vbExclamation, "Parser Error"
    End If
    
    Eval = sB.ToString
    
End Function

Public Function ExecCommand( _
ByRef Obj As Variant) As String
    
    Dim sB As New cStringBuilder
    
    If plNestCount > 40 Then
        
        ExecCommand = "ERROR: Nesting level exceeded."
        
    Else
        
        plNestCount = plNestCount + 1
        
        Select Case VarType(Obj)
            
            Case vbNull
                
                sB.Append "null"
                
            Case vbDate
                
                sB.Append CStr(Obj)
                
            Case vbString
                
                sB.Append CStr(Obj)
                
            Case vbObject
                
                Dim I As Long
                Dim J As Long
                Dim This As Object
                Dim Key
                Dim ParamKeys
                
                If UCase(TypeName(Obj)) = UCase("Dictionary") Then
                    
                    Dim sOut As String
                    Dim sRet As String
                    
                    Dim Keys
                    
                    Keys = Obj.Keys
                    
                    For I = 0 To Obj.Count - 1
                        
                        sRet = Empty
                        
                        Key = Keys(I)
                        
                        If VarType(Obj.Item(Key)) = vbString Then
                            sRet = Obj.Item(Key)
                        Else
                            Set This = Obj.Item(Key)
                        End If
                        
                        ' command implementation
                        
                        Select Case LCase(Key)
                            
                            Case "alert":
                                
                                'MsgBox ExecCommand(this.Item("message")), vbInformation, ExecCommand(this.Item("title"))
                                
                            Case "input":
                                
                                sB.Append InputBox(ExecCommand(This.Item("prompt")), ExecCommand(This.Item("title")), ExecCommand(This.Item("default")))
                                
                            Case "switch"
                                
                                sOut = ExecCommand(This.Item("default"))
                                sRet = LCase(ExecCommand(This.Item("case")))
                                
                                For J = 0 To This.Item("items").Count - 1
                                    If LCase(This.Item("items").Item(J + 1).Item("case")) = sRet Then
                                        sOut = ExecCommand(This.Item("items").Item(J + 1).Item("return"))
                                        Exit For
                                    End If
                                Next
                                
                                sB.Append sOut
                                
                            Case "set":
                                
                                If DictVars.Exists(This.Item("name")) Then
                                    DictVars.Item(This.Item("name")) = ExecCommand(This.Item("value"))
                                Else
                                    DictVars.Add This.Item("name"), ExecCommand(This.Item("value"))
                                End If
                                
                            Case "get":
                                
                                sRet = ExecCommand(DictVars(CStr(This.Item("name"))))
                                
                                If sRet = Empty Then
                                    sRet = ExecCommand(This.Item("default"))
                                End If
                                
                                sB.Append sRet
                                
                            Case "if"
                               
                               Dim Val1 As String
                               Dim Val2 As String
                               Dim bRes As Boolean
                               
                               Val1 = ExecCommand(This.Item("value1"))
                               Val2 = ExecCommand(This.Item("value2"))
                               
                               bRes = False
                               
                               Select Case LCase(This.Item("type"))
                                    
                                    Case "eq" ' =
                                        
                                        If LCase(Val1) = LCase(Val2) Then
                                            bRes = True
                                        End If
                                        
                                    Case "gt" ' >
                                        
                                        If Val1 > Val2 Then
                                            bRes = True
                                        End If
                                        
                                    Case "lt" ' <
                                        
                                        If Val1 < Val2 Then
                                            bRes = True
                                        End If
                                        
                                    Case "gte" ' >=
                                        
                                        If Val1 >= Val2 Then
                                            bRes = True
                                        End If
                                        
                                    Case "lte" ' <=
                                        
                                        If Val1 <= Val2 Then
                                            bRes = True
                                        End If
                                        
                                End Select
                                
                                If bRes Then
                                   sB.Append ExecCommand(This.Item("true"))
                                Else
                                   sB.Append ExecCommand(This.Item("false"))
                                End If
                                
                            Case "return"
                                
                                sB.Append Obj.Item(Key)
                                
                            Case Else
                                
                                If TypeName(This) = "Dictionary" Then
                                    
                                    ParamKeys = This.Keys
                                    
                                    For J = 0 To This.Count - 1
                                        
                                        If J > 0 Then
                                           sRet = sRet & ","
                                        End If
                                        
                                        sRet = sRet & CStr(This.Item(ParamKeys(J)))
                                        
                                    Next
                                    
                                End If
                                
                                sB.Append "<%" & UCase(Key) & "(" & sRet & ")%>"
                                
                        End Select
                        
                    Next I
                    
                ElseIf UCase(TypeName(Obj)) = UCase("Collection") Then
                     
                    Dim Value
                    
                    For Each Value In Obj
                        sB.Append ExecCommand(Value)
                    Next Value
                    
                End If
                
                Set This = Nothing
                
            Case vbBoolean
                
                If Obj Then
                    sB.Append "true"
                Else
                    sB.Append "false"
                End If
                
            Case vbVariant, vbArray, vbArray + vbVariant
                
            Case Else
               
               sB.Append Replace(Obj, ",", ".")
               
        End Select
        
        plNestCount = plNestCount - 1
        
    End If
    
    ExecCommand = sB.ToString
    
    Set sB = Nothing
    
End Function


